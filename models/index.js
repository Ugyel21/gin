const mongoose = require("mongoose");

const ginSchema = new mongoose.Schema({
    issuedTo:{
        required: true,
        type: String
    },
    mailIdOfTheReceiver:{
        required: true,
        type: String
    },
    issuedBy:{
        required: true,
        type: String
    },
    mailIdOfTheIssuer:{
        required: true,
        type: String
    },
    transportedBy:{
        required: true,
        type: String
    },
    invNo:{
        required: true,
        type: Number
    },
    receivedBy:{
        required: true,
        type: String
    },
    invDate:{
        required: true,
        type: Date
    },
});

module.exports = mongoose.model("GIN", ginSchema);