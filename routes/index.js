const express = require("express");
const router = express.Router();

const { getGinPage, postGinPage, getDisplay } = require("../controllers/index");

router.get("/", getGinPage);
router.post("/", postGinPage);

router.get("/display", getDisplay);

module.exports = router;



