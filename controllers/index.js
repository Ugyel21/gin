const ginModel = require("../models/index");

module.exports.getGinPage = async (req, res) => {
  try {
    const success = req.flash("success");
    const error = req.flash("error");
    return res.render("index", {
      success,
      error,
    });
  } catch (error) {
    console.log(error);
    req.flash("error", "An error occurred, please try again!");
    return res.redirect("/");
  }
};

module.exports.postGinPage = async (req, res) => {
  try {
    const {
      issuedTo,
      mailIdOfTheReceiver,
      issuedBy,
      mailIdOfTheIssuer,
      transportedBy,
      invNo,
      receivedBy,
      invDate,
    } = req.body;

    if (
      !issuedTo ||
      !mailIdOfTheReceiver ||
      !issuedBy ||
      !mailIdOfTheIssuer ||
      !transportedBy ||
      !invNo ||
      !receivedBy ||
      !invDate
    ) {
      req.flash("error", "Some fields are empty, Please fill all the fields!");
      return res.redirect("/");
    }

    const newGin = new ginModel({
      issuedTo: issuedTo,
      mailIdOfTheReceiver: mailIdOfTheReceiver,
      issuedBy: issuedBy,
      mailIdOfTheIssuer: mailIdOfTheIssuer,
      transportedBy: transportedBy,
      invNo: invNo,
      receivedBy: receivedBy,
      invDate: invDate,
    });

    newGin
      .save()
      .then(() => {
        req.flash("success", `Your details has been successfully added.`);
        return res.redirect("/");
      })
      .catch((error) => {
        console.log(error);
        req.flash("error", "An error occurred, please try again.");
        return res.redirect("/");
      });
  } catch (error) {
    return res.redirect("/");
  }
};

module.exports.getDisplay = async(req, res) => {
  try{
    const gin = await ginModel.find({});
    return res.render("display", {
      gins: gin || "No records found!",
    });
  }catch(error){
    error.log(error);
  }
}
